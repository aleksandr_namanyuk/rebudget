package nordea.junction.rebudget.utils;

import nordea.junction.rebudget.R;

/**
 * Application constant values.
 *
 * Created by Bel on 25/11/2017.
 */

public class Constants {

    public static final int CATEGORY_FOOD = 1;
    public static final int CATEGORY_HOUSING = 2;
    public static final int CATEGORY_TRANSPORTATION = 3;
    public static final int CATEGORY_TRAVEL = 4;
    public static final int CATEGORY_LIFE = 5;
    public static final int CATEGORY_COMMUNICATION = 6;
    public static final int CATEGORY_FINANCIAL_EXPENSES = 7;
    public static final int CATEGORY_EDUCATION = 8;
    public static final int CATEGORY_RESTAURANTS = 9;
    public static final int CATEGORY_COFFEE = 10;

    public static final int CATEGORY_ICON_FOOD = R.drawable.groceries;
    public static final int CATEGORY_ICON_HOUSING = R.drawable.housing;
    public static final int CATEGORY_ICON_TRANSPORTATION = R.drawable.vehicle;
    public static final int CATEGORY_ICON_TRAVEL = R.drawable.transportation;
    public static final int CATEGORY_ICON_LIFE = R.drawable.lifeentertainment;
    public static final int CATEGORY_ICON_COMMUNICATION = R.drawable.communications;
    public static final int CATEGORY_ICON_FINANCIAL_EXPENSES = R.drawable.financialexpenses;
    public static final int CATEGORY_ICON_EDUCATION = R.drawable.education;
    public static final int CATEGORY_ICON_RESTAURANTS = R.drawable.restaurants;
    public static final int CATEGORY_ICON_COFFEE = R.drawable.coffee;

    public static final String CATEGORY_TITLE_FOOD = "Groceries";
    public static final String CATEGORY_TITLE_HOUSING = "Housing";
    public static final String CATEGORY_TITLE_TRANSPORTATION = "Transportation";
    public static final String CATEGORY_TITLE_TRAVEL = "Travelling";
    public static final String CATEGORY_TITLE_LIFE = "Life";
    public static final String CATEGORY_TITLE_COMMUNICATION = "Communication";
    public static final String CATEGORY_TITLE_FINANCIAL_EXPENSES = "Financal Expenses";
    public static final String CATEGORY_TITLE_EDUCATION = "Education";
    public static final String CATEGORY_TITLE_RESTAURANTS = "Restaurants";
    public static final String CATEGORY_TITLE_COFFEE = "Coffee";

    public static final String PREF_GOAL_VALUE = "pref_goal_value";
    public static final String PREF_MONTHS_SAVED = "months_saved";

    public static final int ICON_MENU_ACCOUNT_LIST = R.drawable.ic_account_balance_wallet_black_24dp;
    public static final int ICON_ADD_GOAL = R.drawable.ic_add_black_24dp;


}
